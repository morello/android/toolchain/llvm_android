#
# Copyright (C) 2017 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import paths
import re
import sys
import utils

_llvm_next = False
_version_read = False

_patch_level = '0'
_remote_name = ''
_branch_name = ''
_branch_name_modifier = ''
_svn_revision = ''


def _get_llvm_remote_and_branch_info():
    global _remote_name
    global _branch_name
    global _branch_name_modifier

    if _branch_name == '':
        llvm_repo_dir = paths.TOOLCHAIN_LLVM_PATH
        remote_refs = []

        # Get the current upstream branch, if there is one.
        get_upstream_res = utils.subprocess_run(
                ['git', 'rev-parse', '--symbolic-full-name', '@{upstream}'],
                cwd=llvm_repo_dir, capture_output=True)

        if get_upstream_res.returncode == 0:
            remote_refs.append(get_upstream_res.stdout.strip())
        else:
            # If not, get the upstream configured in the repo manifest (there
            # should be only one). m/* is a symbolic reference to the upstream
            # branch, so we need %(symref) to get the actual branch name.
            remote_refs.extend(utils.check_output(
                    ['git', 'for-each-ref', 'refs/remotes/m/**', '--format', '%(symref)'],
                    cwd=llvm_repo_dir).splitlines())

        number_of_commits_added_candidate = -1
        ref_candidate = ''

        for remote_ref in remote_refs:
            remote_ref_commit_hash = utils.check_output(
                    ['git', 'rev-parse', remote_ref],
                    cwd=llvm_repo_dir).strip()

            number_of_commits_added_str = utils.check_output(
                    ['git', 'rev-list', '--count', '%s..%s' % (remote_ref, 'HEAD')],
                    cwd=llvm_repo_dir).strip()
            number_of_commits_added = int(number_of_commits_added_str)

            if ref_candidate == '' or number_of_commits_added_candidate > number_of_commits_added:
                ref_candidate = remote_ref
                number_of_commits_added_candidate = number_of_commits_added

        if ref_candidate == '':
            raise Exception('No remote ref found.')

        branch_name_suffixes = []
        if number_of_commits_added_candidate != 0:
            head_short_commit_hash = utils.check_output(
                    ['git', 'rev-parse', '--short', 'HEAD'],
                    cwd=llvm_repo_dir).strip()

            branch_name_suffixes += ['%s-g%s' % (number_of_commits_added_candidate, head_short_commit_hash)]

        is_clean = not utils.check_output(['git', 'status', '-z'], cwd=llvm_repo_dir)

        if not is_clean:
            branch_name_suffixes += ['modified']

        match = re.match('refs\/remotes\/([^/]+)\/(.+)', ref_candidate)

        if match == None:
            raise ValueError("Can't parse branch name: '" + full_branch_name + "'")

        _remote_name = match.group(1)
        _branch_name = match.group(2)
        _branch_name_modifier = '-'.join(branch_name_suffixes)

    return _remote_name, _branch_name, _branch_name_modifier

def _get_svn_revision():
    if _llvm_next:
        raise ValueError('build_llvm_next mode unsupported')

    global _svn_revision

    if _svn_revision == '':
        llvm_repo_dir = paths.TOOLCHAIN_LLVM_PATH

        if is_llvm_repo_shallow():
            # Can't determine SVN revision from shallow repository
            _svn_revision='svn-revision-unknown'
        else:
            remote_name, branch_name, branch_name_modifier = _get_llvm_remote_and_branch_info()

            git_llvm_rev_cmd = [
                sys.executable,
                paths.TOOLCHAIN_UTILS_DIR / 'llvm_tools' / 'git_llvm_rev.py',
                '--llvm_dir', llvm_repo_dir,
                '--upstream', remote_name,
                '--sha', 'HEAD'
            ]

            _svn_revision = utils.check_output(git_llvm_rev_cmd).strip()

    return _svn_revision

def is_llvm_repo_shallow():
    llvm_repo_dir = paths.TOOLCHAIN_LLVM_PATH

    is_shallow_repository_res = utils.subprocess_run(
            ['git', 'rev-parse', '--is-shallow-repository'],
            cwd=llvm_repo_dir, capture_output=True)

    return (is_shallow_repository_res.returncode == 0 and
            is_shallow_repository_res.stdout.strip() == 'true')

def get_svn_branch_revision_tuple():
    remote_name, branch_name, branch_name_modifier = _get_llvm_remote_and_branch_info()
    svn_revision = _get_svn_revision()

    return branch_name, branch_name_modifier, svn_revision

def get_svn_branch_revision_str():
    branch_name, branch_name_modifier, svn_revision = get_svn_branch_revision_tuple()

    revision_str = branch_name

    if branch_name_modifier:
        revision_str += '-' + branch_name_modifier

    revision_str += '-' + svn_revision

    return revision_str


def set_llvm_next(llvm_next: bool):
    if _version_read:
        raise RuntimeError('set_llvm_next() after earlier read of versions')
    # pylint:disable=global-statement
    global _llvm_next
    _llvm_next = llvm_next


def override_branch_name(branch_name: str):
    global _branch_name
    global _svn_revision
    _branch_name = branch_name
    _svn_revision = 'svn-revision-unknown'


def is_llvm_next() -> bool:
    _version_read = True
    return _llvm_next


def get_patch_level():
    _version_read = True
    if _llvm_next:
        return None
    return _patch_level


def get_svn_revision_number():
    """Get the numeric portion of the version number we are working with.
       Strip the leading 'r' and possible letter (and number) suffix,
       e.g., r383902b1 => 383902
    """
    svn_version = _get_svn_revision()
    found = re.match(r'r(\d+)([a-z]\d*)?$', svn_version)
    if not found:
        raise RuntimeError(f'Invalid svn revision: {svn_version}')
    return found.group(1)
